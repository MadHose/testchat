const configDevelopment = require("./config.dev");
const log = require("../services/logger")("CONFIG");
log.info(`Server started in ${process.env.NODE_ENV}`);
module.exports = configDevelopment;