const Koa = require("koa"),
  logger = require("koa-logger"),
  helmet = require("koa-helmet"),  
  compress = require("koa-compress"),  
  bodyParser = require("koa-bodyparser"),
  { routes, allowedMethods } = require("./middleware/routes"),
  { websockified } = require("./middleware/wsroutes"),
  log = require("./services/logger")("APP"),
  { port } = require("./config");

const PORT = port || 3000;

const app = new Koa()

 
app
  .use(helmet())
  .use(compress())
  .use(logger())
  .use(bodyParser())
  .use(routes())
  .use(allowedMethods());


app.on("error", (err, ctx) => console.error("Custom error handler", err, ctx));

log.info(`App started on ${PORT} port`);

const server = app.listen(PORT);

websockified(server);