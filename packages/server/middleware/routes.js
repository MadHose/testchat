const Router = require("koa-router");
const { login } = require("../controllers/authentication");

const router = new Router();
module.exports.router = router;

router.post("/login", login);

exports.routes = () => router.routes();
exports.allowedMethods = () => router.allowedMethods();