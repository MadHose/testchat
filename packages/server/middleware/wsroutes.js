const socket = require('socket.io'),
      moment = require('moment'),
      userWs = module.exports = {},
      log = require("../services/logger")("WSROUTES");

module.exports.websockified = app => {
    const wsapp = socket(app);
    
    wsapp.on('connection', function(socket){
        let {name, id} = socket.handshake.query;
        if (id === 'new'){
          id = socket.id;
        }
        if (userWs[id] === undefined){
          userWs[id] = []
        }
        log.info(`User ${name} connected to ${id} room`);
        socket.join(id)
        socket.emit('initUserInfo', {id, users:userWs[id]})
        userWs[id].push(name)
        wsapp.to(id).emit('newUser',{name})        
        socket.on('newMessage', function(socket){
          if (socket.text) {
          let time = moment().format('MMMM Do YYYY, h:mm:ss a');
          wsapp.to(id).emit('chatMessage',{...socket, name, time })
            } 
        })
        socket.on('disconnect', function(socket){
            userWs[id].shift(name)
          })
      })
    

    return wsapp
}
