const userWs = require("../middleware/wsroutes");

/**
 * Контроллер для обработки запроса аутентификации
 */

exports.login = async (ctx, next) => {
  let {name, id} = ctx.request.body
  let check = false;
  if ((id === "new" || userWs[id].indexOf(name) === -1) && name){
    check = true
  }
  if (check){
      ctx.status = 200;
      ctx.body = {
        result: true,
        msg: "authentication succeeded"
      };
    }
  else {
    ctx.status = 401;
    ctx.body = {
      result: false,
      msg: "name allready in use"
    }

  }
};