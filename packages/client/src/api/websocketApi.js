import openSocket from 'socket.io-client';
import { store } from '..';
import { addNewMessage } from '../actions/chat';
import { initUserInfo, addNewUser } from '../actions/auth';

/**
 * Базовый экземпляр SocketWorker
 */

export default class SocketWorker {
    constructor(url) {
        this.url = url;
        this.socket = undefined;
    }
    
    /**
     * Открывает сокет-соединение, принимает на вход имя пользователя
     * @param name
     */
    operConnect = (name) =>{
        const { dispatch } = store;
        this.socket = openSocket(this.url, {query:name});
        this.socket.on('chatMessage', function(msg){
            dispatch(addNewMessage(msg))
        })

        this.socket.on('initUserInfo', function(msg){
          dispatch(initUserInfo(msg))
        })

        this.socket.on('newUser', function(msg){
          dispatch(addNewUser(msg))
        })
    }
    /**
     * Отправляет сообщение, принимает на вход тип сообщения и данные
     * @param type
     * @param msg
     */
    sendMessage = (type, msg) => {
        if (this.socket !== undefined) {
          this.socket.emit(type, msg);
        } else {
          throw new Error('Объект SocketWorker не инициализирован');
        }
      };
}