import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { configureStore, history } from "./redux";
import { Provider } from "react-redux";
import * as serviceWorker from './serviceWorker';
import SocketWorker from './api/websocketApi';
import "antd/dist/antd.css";
import "./styles/index.scss";

export const store = configureStore();

export const socket = new SocketWorker('http://localhost:5000')

ReactDOM.render(
    <Provider store={store}>
        <App history={history}/>
    </Provider>, document.getElementById('root'));

serviceWorker.unregister();
