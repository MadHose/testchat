import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import  loginReducer from './loginReducer';
import  chatReducer from './chatReducer';

/**
 * Обьединяет редьюсеры и создаёт стор приложения
 */

const createStore = history => combineReducers({
  loginReducer,
  chatReducer,
  router: connectRouter(history)
})

const rootReducer = history => (state, action) => {
    return createStore(history)(state, action);
  };

export default rootReducer