const initialState = { messages: []};

/**
 * Редьюсер обрабатывающий события пришедшие во время работы чата
 */

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_NEW_MESSAGE': {
      return {
        ...state,
        messages: [...state.messages,{...action.payload}]
      };
    }
    default:
      return state;
  }
};

export default chatReducer;