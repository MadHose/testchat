const initialState = { isAuthenticated: false, users:[]};

/**
 * Редьюсер обрабатывающий события пришедшие во время аутентефикации
 */

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'AUTH_SUCCESS': {
      return {
        ...state,
        isAuthenticated: true
      };
    }
    case 'INIT_USER_INFO': {
      return {
        ...state,
        ...action.payload
      };
    }
    case 'ADD_NEW_USER': {
      return {
        ...state,
        users: [...state.users, action.payload.name]
      };
    }
    default:
      return state;
  }
};

export default loginReducer;