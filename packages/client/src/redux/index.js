import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import rootSaga from "./sagas";
import createRootReducer from "./reducers/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";

export const history = createBrowserHistory();

export const configureStore = () => {
    const sagaMiddleware = createSagaMiddleware(history);
    const composedMiddleware = composeWithDevTools(applyMiddleware(sagaMiddleware, routerMiddleware(history)));
    const store = createStore(createRootReducer(history), composedMiddleware);
    sagaMiddleware.run(rootSaga);

    return store;
  };