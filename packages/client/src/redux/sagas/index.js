import { all } from "redux-saga/effects";
import userSaga from "./userSaga";

/**
 * Создание общей саги приложения
 */

export default function* rootSaga() {
    yield all([userSaga()]);
  }