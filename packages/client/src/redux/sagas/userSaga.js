import axios from 'axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import { authSuccessAction } from '../../actions/auth';
import { notification } from 'antd';
import { socket } from '../../index';

/**
 * Сага для обработки пользовательских запросов
 */
function login(data) {
    return axios.post('/login', data);
}

function* authRequest(action){
    try {
        const response = yield call(login, action.payload)
        if (response.data.result){
            socket.operConnect(action.payload);
            yield put(authSuccessAction(action.payload))
            
        } else {
            notification.error({
                message:"Ошибка авторизации",
                duration:2
            })
        }
    }
    catch(e){
        notification.error({
            message:"Ошибка при авторизации",
            duration:2
        })
    }
}

export default function* watchUserSaga() {
    yield takeLatest("AUTH_REQUEST", authRequest);
  }