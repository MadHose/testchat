import React from 'react';
import { connect } from 'react-redux';
import { Input, Button } from 'antd';
import { socket } from '../index';

const ChatLauout = ({chatMessages, users, id}) => {
    const [message, setMessage] = React.useState('');

    function changeMessage(e){
        setMessage(e.target.value)
    }

    function sendMessage(){
        socket.sendMessage('newMessage',{text:message});
        setMessage('')
    }
    
    return(
    <div className="chatDisplay">
        <div className='chat'>
                { <div>{chatMessages.map((msg, index)=> <div className = 'message' key = {index}><p>{msg.time}</p><p>{msg.name}</p><p>{msg.text}</p></div>)}</div>  }
                { <div> <p>Пользователи</p>{users.map((user)=><p>{user}</p>)}</div> }
        </div>
        <div className='inputBody'>
            <div className='input'>
                <Input 
                    name="name" 
                    placeholder="Введите сообщение" 
                    onChange={changeMessage}
                    value={message}
                />
                <Button
                    type="primary"
                    onClick={sendMessage}
                    htmlType="submit"
                >
                    Отправить
                </Button>
            </div>
            <div className='source'>
            Cсылка на чат
            <Input 
                name="name"
                disabled={true}
                placeholder="Cсылка на чат" 
                value={`localhost:3000/id${id}`}
            />
            </div>
        </div>
    </div>
    )
}

function mapStateToProps(state) {
    return {
        chatMessages: state.chatReducer.messages,
        users:state.loginReducer.users,
        id: state.loginReducer.id
    };
}

export default connect(mapStateToProps)(ChatLauout);