import React from 'react';
import { Input, Button } from 'antd';
import { connect } from 'react-redux';
import { useParams } from 'react-router';
import { authRequestAction } from '../actions/auth'

const LoginLayout = ({auth}) => {
    const [name, setName] = React.useState('');
    let { id='new' } = useParams();

    function changeName(e){
        setName(e.target.value)
    }

    function loginRequest(){
        auth({name, id})
    }

    return (
        <div className="login">
            <Input className="input" name="name" placeholder="Введите имя для чата" onChange={changeName}/>
            <Button
                type="primary"
                onClick={loginRequest}
                htmlType="submit"
            >
                Войти
            </Button>
        </div>
    )
};
const mapDispatchToProps = dispatch => ({
    auth: state => dispatch(authRequestAction(state))
  });
  
export default connect(null,mapDispatchToProps)(LoginLayout);