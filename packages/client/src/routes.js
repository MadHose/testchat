import React from 'react';
import { Route, Switch,Redirect } from 'react-router';
import { connect } from 'react-redux';
import LoginLayout from './layouts/LoginLayout';
import ChatLayout from './layouts/ChatLayout';

/**
 * Внутренний роутер приложения, проверяет аутентефикацию пользователя, выдавая чат или страницу логина
 */

const Routes = ({isAuthenticated}) => (
    <>
    {
        isAuthenticated ? (
            <>
                <Redirect to="/" />
                <Route exact path="/" component={ChatLayout} />
            </>
            ):(
            <Switch>
                <Route exact path="/" component={LoginLayout} />
                <Route path="/id:id" component ={LoginLayout} />
            </Switch>
            )
    }
    </>
)

const mapStateToProps = state => ({
    isAuthenticated: state.loginReducer.isAuthenticated
  });

export default connect(mapStateToProps)(Routes);