/**
 * Экшены вызываемые во время работы с чатом
 */

export const addNewMessage = state => (
    {
      type: "ADD_NEW_MESSAGE",
      payload: {
        ...state
      }
});

