/**
 * Экшены вызываемые во время аутентефикации
 */

export const authRequestAction = state => (
    {
      type: "AUTH_REQUEST",
      payload: {
        ...state
      }
    });
    
export const authSuccessAction = () => (
  {
    type: "AUTH_SUCCESS", 
});

export const initUserInfo = state => (
  {
    type: "INIT_USER_INFO",
    payload: {
      ...state
    }
  });

export const addNewUser = state => (
  {
    type: "ADD_NEW_USER",
    payload: {
      ...state
    }
});